import dataclasses
import datetime
import decimal

from crypto_converter.domain.models import Quote
from crypto_converter.storage.repository import QuoteRepository


class QuotesFetchingError(Exception):
    pass


class UnsupportedConversionError(QuotesFetchingError):
    pass


class OutdatedQuotesError(QuotesFetchingError):
    pass


@dataclasses.dataclass(kw_only=True)
class ConverterService:
    expiration_interval: datetime.timedelta
    precision: int

    quotes: QuoteRepository

    async def convert(
        self,
        amount: decimal.Decimal,
        from_currency: str,
        to_currency: str,
        timestamp: datetime.datetime,
    ) -> tuple[decimal.Decimal, Quote]:
        quote = await self._get_quote(
            from_currency=from_currency,
            to_currency=to_currency,
            timestamp=timestamp,
        )
        return round(amount * quote.rate, self.precision), quote

    async def _get_quote(
        self,
        timestamp: datetime.datetime,
        from_currency: str,
        to_currency: str,
    ) -> Quote:
        latest_quote = await self.quotes.get_latest_quote(
            from_currency=from_currency,
            to_currency=to_currency,
            timestamp=timestamp,
        )

        if latest_quote is None:
            latest_reversed_quote = await self.quotes.get_latest_quote(
                from_currency=to_currency,
                to_currency=from_currency,
                timestamp=timestamp,
            )

            if latest_reversed_quote is None:
                raise UnsupportedConversionError()

            latest_quote = Quote(
                id=latest_reversed_quote.id,
                from_currency=from_currency,
                to_currency=to_currency,
                timestamp=latest_reversed_quote.timestamp,
                rate=round(1 / latest_reversed_quote.rate, 2 * self.precision),
            )

        if timestamp - latest_quote.timestamp >= self.expiration_interval:
            raise OutdatedQuotesError()

        return latest_quote
