import dataclasses
import datetime
import decimal
import uuid


@dataclasses.dataclass(kw_only=True)
class Quote:
    id: uuid.UUID
    rate: decimal.Decimal
    timestamp: datetime.datetime
    from_currency: str
    to_currency: str
