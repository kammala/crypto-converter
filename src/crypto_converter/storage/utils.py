import contextlib
from typing import AsyncIterator

from sqlalchemy.ext import asyncio as sa_async

from crypto_converter.storage.settings import StorageSettings


@contextlib.asynccontextmanager
async def get_engine(settings: StorageSettings) -> AsyncIterator[sa_async.AsyncEngine]:
    engine = sa_async.create_async_engine(settings.dsn)
    yield engine
    await engine.dispose()
