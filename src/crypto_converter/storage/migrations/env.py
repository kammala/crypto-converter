from logging.config import fileConfig

import sqlalchemy
from alembic import context
from crypto_converter.storage.models import Base
from crypto_converter.storage.settings import StorageSettings
from sqlalchemy.ext.asyncio import create_async_engine

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

target_metadata = Base.metadata


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = StorageSettings().dsn
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


async def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    url = StorageSettings().dsn
    engine = create_async_engine(url)

    connection_ready = await is_db_connection_ready()
    if not connection_ready:
        raise SystemExit("Database is not ready")

    def do_run_migrations(connection: sqlalchemy.Connection) -> None:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()

    async with engine.connect() as conn:
        await conn.run_sync(do_run_migrations)


async def is_db_connection_ready() -> bool:
    url = StorageSettings().dsn
    engine = create_async_engine(url)
    try:
        async with engine.connect() as connection:
            (await connection.execute(sqlalchemy.select(1))).all()
        return True
    except Exception:
        return False
    finally:
        await engine.dispose()


if context.is_offline_mode():
    run_migrations_offline()
else:
    import asyncio

    asyncio.run(run_migrations_online())
