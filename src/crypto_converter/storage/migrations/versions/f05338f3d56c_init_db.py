"""init db

Revision ID: f05338f3d56c
Revises:
Create Date: 2024-02-12 16:38:57.411579

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "f05338f3d56c"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        "quotes",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("timestamp", sa.TIMESTAMP(timezone=True), nullable=False),
        sa.Column("from_currency", sa.String(), nullable=False),
        sa.Column("to_currency", sa.String(), nullable=False),
        sa.Column("rate", sa.Numeric(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade() -> None:
    op.drop_table("quotes")
