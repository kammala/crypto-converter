import abc
import datetime

import sqlalchemy as sa
from sqlalchemy.ext.asyncio import AsyncSession

from crypto_converter.domain.models import Quote
from crypto_converter.storage.models import QuotePostgresModel


class QuoteRepository(abc.ABC):
    @abc.abstractmethod
    async def get_latest_quote(
        self,
        *,
        from_currency: str,
        to_currency: str,
        timestamp: datetime.datetime,
    ) -> Quote | None:
        pass

    @abc.abstractmethod
    async def add(self, quote: Quote) -> None:
        pass


class PostgresQuoteRepository(QuoteRepository):
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get_latest_quote(
        self,
        *,
        from_currency: str,
        to_currency: str,
        timestamp: datetime.datetime,
    ) -> Quote | None:
        query = (
            sa.select(QuotePostgresModel)
            .where(
                QuotePostgresModel.from_currency == from_currency,
                QuotePostgresModel.to_currency == to_currency,
                QuotePostgresModel.timestamp <= timestamp,
            )
            .order_by(sa.desc(QuotePostgresModel.timestamp))
            .limit(limit=1)
        )
        results = (await self._session.execute(query)).scalars().all()

        if not results:
            return None

        stored_quote = results[0]
        return Quote(
            id=stored_quote.id,
            timestamp=stored_quote.timestamp,
            from_currency=stored_quote.from_currency,
            to_currency=stored_quote.to_currency,
            rate=stored_quote.rate,
        )

    async def add(self, quote: Quote) -> None:
        await self._session.merge(
            QuotePostgresModel(
                id=quote.id,
                timestamp=quote.timestamp,
                from_currency=quote.from_currency,
                to_currency=quote.to_currency,
                rate=quote.rate,
            )
        )
        await self._session.flush()
