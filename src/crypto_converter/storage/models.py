import datetime
import decimal
import uuid
from typing import ClassVar

from sqlalchemy import TIMESTAMP
from sqlalchemy.orm import DeclarativeBase, Mapped, MappedAsDataclass, mapped_column


class Base(MappedAsDataclass, DeclarativeBase, kw_only=True):
    type_annotation_map: ClassVar[dict[type[object], object]] = {
        datetime.datetime: TIMESTAMP(timezone=True)
    }


class QuotePostgresModel(Base):
    __tablename__ = "quotes"

    id: Mapped[uuid.UUID] = mapped_column(primary_key=True)

    timestamp: Mapped[datetime.datetime] = mapped_column(nullable=False)
    from_currency: Mapped[str] = mapped_column(nullable=False)
    to_currency: Mapped[str] = mapped_column(nullable=False)
    rate: Mapped[decimal.Decimal] = mapped_column(nullable=False)
