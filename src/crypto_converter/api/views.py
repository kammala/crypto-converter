import datetime
import decimal
import http
from collections.abc import AsyncIterator
from typing import Annotated

import fastapi
import sqlalchemy.ext.asyncio as sa_async

from crypto_converter.api.models import (
    ConversionResult,
    ErrorCode,
    ErrorData,
    ErrorResponse,
    SuccessfulResponse,
)
from crypto_converter.api.settings import APISettings
from crypto_converter.domain.converter import (
    ConverterService,
    OutdatedQuotesError,
    UnsupportedConversionError,
)
from crypto_converter.storage.repository import PostgresQuoteRepository, QuoteRepository
from crypto_converter.storage.settings import StorageSettings

router = fastapi.APIRouter()


def get_config() -> APISettings:
    return APISettings()


def get_storage_config() -> StorageSettings:
    return StorageSettings()


async def get_quotes_repository(
    storage_config: Annotated[StorageSettings, fastapi.Depends(get_storage_config)],
) -> AsyncIterator[QuoteRepository]:
    engine = sa_async.create_async_engine(storage_config.dsn)
    session_factory = sa_async.async_sessionmaker(engine)
    async with session_factory() as session:
        yield PostgresQuoteRepository(session=session)
    await engine.dispose()


@router.get(
    "/convert",
    description=(
        "Convert the `amount` of `from` currency to `to` currency "
        "using the latest rate at `at` moment in time."
    ),
    responses={
        http.HTTPStatus.OK: {"model": SuccessfulResponse},
        http.HTTPStatus.NOT_FOUND: {"model": ErrorResponse},
    },
)
async def convert(  # noqa: PLR0913
    repo: Annotated[QuoteRepository, fastapi.Depends(get_quotes_repository)],
    response: fastapi.Response,
    config: Annotated[APISettings, fastapi.Depends(get_config)],
    amount: Annotated[decimal.Decimal, fastapi.Query(decimal_places=6)],
    from_currency: Annotated[str, fastapi.Query(alias="from")],
    to_currency: Annotated[str, fastapi.Query(alias="to")],
    at: Annotated[
        int | None,
        fastapi.Query(
            description=(
                "UNIX timestamp multiplied by 1000, like in Javascript. "
                "Defaults to now if not provided."
            )
        ),
    ] = None,
) -> object:
    timestamp = (
        datetime_from_ts(at)
        if at is not None
        else datetime.datetime.now(tz=datetime.timezone.utc)
    )

    converter = ConverterService(
        quotes=repo,
        expiration_interval=datetime.timedelta(
            seconds=config.QUOTES_EXPIRATION_INTERVAL
        ),
        precision=config.CONVERSION_PRECISION,
    )
    try:
        result, quote = await converter.convert(
            amount=amount,
            from_currency=from_currency.upper(),
            to_currency=to_currency.upper(),
            timestamp=timestamp,
        )
        return SuccessfulResponse(
            data=ConversionResult(
                amount=result,
                conversion_rate=quote.rate,
            )
        )
    except UnsupportedConversionError:
        response.status_code = http.HTTPStatus.NOT_FOUND
        return ErrorResponse(
            errors=ErrorData(code=ErrorCode.currency_not_supported),
        )
    except OutdatedQuotesError:
        response.status_code = http.HTTPStatus.NOT_FOUND
        return ErrorResponse(
            errors=ErrorData(code=ErrorCode.quotes_outdated),
        )


def datetime_from_ts(ts: int) -> datetime.datetime:
    return datetime.datetime.fromtimestamp(ts / 1000, tz=datetime.timezone.utc)
