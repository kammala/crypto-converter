import decimal
import enum

import pydantic


class ErrorCode(enum.StrEnum):
    quotes_outdated = enum.auto()
    currency_not_supported = enum.auto()


class ConversionRequest(pydantic.BaseModel):
    model_config = pydantic.ConfigDict()


class ConversionResult(pydantic.BaseModel):
    amount: decimal.Decimal
    conversion_rate: decimal.Decimal


class SuccessfulResponse(pydantic.BaseModel):
    data: ConversionResult


class ErrorData(pydantic.BaseModel):
    code: ErrorCode


class ErrorResponse(pydantic.BaseModel):
    errors: ErrorData
