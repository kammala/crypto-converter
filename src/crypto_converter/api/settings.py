import pydantic_settings


class APISettings(pydantic_settings.BaseSettings):
    model_config = pydantic_settings.SettingsConfigDict(env_prefix="API_")

    HOST: str
    PORT: int

    QUOTES_EXPIRATION_INTERVAL: int
    CONVERSION_PRECISION: int

    RELOAD: bool = False
