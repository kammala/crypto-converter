import logging

import fastapi
import uvicorn

from crypto_converter.api import settings, views

logger = logging.getLogger(__name__)


def create_app() -> fastapi.FastAPI:
    app = fastapi.FastAPI()

    app.include_router(views.router)

    return app


def run() -> None:
    config = settings.APISettings()

    uvicorn.run(
        "crypto_converter.api.app:create_app",
        host=config.HOST,
        port=config.PORT,
        reload=config.RELOAD,
        factory=True,
    )


if __name__ == "__main__":
    run()
