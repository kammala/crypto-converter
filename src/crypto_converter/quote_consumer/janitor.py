import asyncio
import datetime
import logging

import sqlalchemy as sa
from sqlalchemy.ext import asyncio as sa_async

from crypto_converter.storage.models import QuotePostgresModel
from crypto_converter.storage.settings import StorageSettings
from crypto_converter.storage.utils import get_engine

logger = logging.getLogger(__name__)


async def cleanup_quotes(
    interval: int,
    max_age: datetime.timedelta,
    storage_config: StorageSettings,
) -> None:
    try:
        async with get_engine(storage_config) as engine:
            session_factory = sa_async.async_sessionmaker(engine)
            while True:
                await asyncio.sleep(interval)
                async with session_factory() as session:
                    logger.info("cleaning up quotes")
                    outdated_ts = (
                        datetime.datetime.now(tz=datetime.timezone.utc) - max_age
                    )
                    query = sa.delete(QuotePostgresModel).where(
                        QuotePostgresModel.timestamp < outdated_ts
                    )
                    await session.execute(query)
                    await session.commit()
    except Exception as ex:
        logger.exception(ex)
        raise
