import asyncio
import datetime
import logging
import uuid
from collections.abc import Iterable

import sqlalchemy.ext.asyncio as sa_async

from crypto_converter.domain.models import Quote
from crypto_converter.quote_consumer.binance.client import QuotesClient
from crypto_converter.quote_consumer.binance.settings import BinanceSettings
from crypto_converter.quote_consumer.janitor import cleanup_quotes
from crypto_converter.quote_consumer.settings import ConsumerSettings
from crypto_converter.storage.repository import PostgresQuoteRepository
from crypto_converter.storage.settings import StorageSettings
from crypto_converter.storage.utils import get_engine

logger = logging.getLogger(__name__)


class TempQuotesRepo:
    _storage: dict[tuple[str, str], Quote]

    def __init__(self) -> None:
        self._storage = {}

    def get_latest_quotes(self) -> Iterable[Quote]:
        return self._storage.values()

    def add(self, quote: Quote) -> None:
        self._storage[(quote.from_currency, quote.to_currency)] = quote


async def dump_repo_to_db(
    temp_repo: TempQuotesRepo, interval: int, storage_config: StorageSettings
) -> None:
    try:
        async with get_engine(storage_config) as engine:
            session_factory = sa_async.async_sessionmaker(engine)
            while True:
                await asyncio.sleep(interval)
                async with session_factory() as session:
                    logger.info("dumping state to db")
                    for quote in temp_repo.get_latest_quotes():
                        await PostgresQuoteRepository(session=session).add(quote=quote)
                    await session.commit()
    except Exception as ex:
        logger.exception(ex)
        raise


async def main() -> None:
    # todo: convert into fully-fledged service with proper setup and graceful shutdown
    consumer_config = ConsumerSettings()
    binance_config = BinanceSettings()
    quotes_client = QuotesClient(config=binance_config)

    repo = TempQuotesRepo()

    symbols = await quotes_client.get_available_symbols(consumer_config.conversions)
    _db_dumper = asyncio.create_task(  # noqa: RUF006
        dump_repo_to_db(
            temp_repo=repo,
            interval=consumer_config.DB_DUMP_INTERVAL,
            storage_config=StorageSettings(),
        ),
        name="db_dumper",
    )
    # note: ideally janitor should be a separate deployable unit
    #  running as cronjob and not consuming any resources
    _janitor = asyncio.create_task(  # noqa: RUF006
        cleanup_quotes(
            interval=consumer_config.DB_CLEANUP_INTERVAL,
            max_age=datetime.timedelta(seconds=consumer_config.QUOTES_MAX_AGE),
            storage_config=StorageSettings(),
        ),
        name="quotes_janitor",
    )

    async for update in quotes_client.subscribe(list(symbols)):
        from_currency, to_currency = symbols[update.symbol]
        logger.info("update for (%s, %s): %s", from_currency, to_currency, update.price)
        repo.add(
            Quote(
                id=uuid.uuid4(),
                from_currency=from_currency,
                to_currency=to_currency,
                timestamp=datetime.datetime.fromtimestamp(
                    update.event_timestamp / 1000,
                    tz=datetime.timezone.utc,
                ),
                rate=round(update.price, consumer_config.RATES_PRECISION),
            )
        )


def run() -> None:
    asyncio.run(main())
