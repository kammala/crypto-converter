import decimal
import uuid
from typing import Literal, Self

import pydantic


class SubscribeRequest(pydantic.BaseModel):
    id: uuid.UUID
    method: Literal["SUBSCRIBE"] = "SUBSCRIBE"
    params: list[str]

    @classmethod
    def for_symbols(cls, symbols: list[str]) -> Self:
        return cls(
            id=uuid.uuid4(),
            method="SUBSCRIBE",
            params=[f"{symbol.lower()}@avgPrice" for symbol in symbols],
        )


class GetExchangeInfoRequest(pydantic.BaseModel):
    id: uuid.UUID = pydantic.Field(default_factory=uuid.uuid4)
    method: Literal["exchangeInfo"] = "exchangeInfo"


class SymbolData(pydantic.BaseModel):
    symbol: str
    from_currency: str = pydantic.Field(alias="baseAsset")
    to_currency: str = pydantic.Field(alias="quoteAsset")


class ExchangeInfo(pydantic.BaseModel):
    symbols: list[SymbolData]


class GetExchangeInfoResponse(pydantic.BaseModel):
    id: uuid.UUID = pydantic.Field(default_factory=uuid.uuid4)
    result: ExchangeInfo


class SubscriptionResponse(pydantic.BaseModel):
    id: uuid.UUID
    result: None


class AveragePriceInfoEvent(pydantic.BaseModel):
    event_type: Literal["avgPrice"] = pydantic.Field("avgPrice", alias="e")
    event_timestamp: int = pydantic.Field(alias="E")
    symbol: str = pydantic.Field(alias="s")
    price: decimal.Decimal = pydantic.Field(alias="w")


def parse(data: object) -> AveragePriceInfoEvent | SubscriptionResponse:
    return (
        pydantic.RootModel[AveragePriceInfoEvent | SubscriptionResponse]
        .model_validate(data)
        .root
    )
