import json
import logging
import uuid
from typing import AsyncIterator

import pydantic
import websockets

from crypto_converter.quote_consumer.binance import models
from crypto_converter.quote_consumer.binance.settings import BinanceSettings

logger = logging.getLogger(__name__)


class Encoder(json.JSONEncoder):
    def default(self, o: object) -> object:
        if isinstance(o, uuid.UUID):
            return str(o)
        if isinstance(o, pydantic.BaseModel):
            return o.model_dump()
        return super().default(o)


class QuotesClient:
    def __init__(self, config: BinanceSettings) -> None:
        self._config = config

    async def get_available_symbols(
        self,
        currencies: list[tuple[str, str]],
    ) -> dict[str, tuple[str, str]]:
        async with websockets.connect(
            self._config.WS_API_URL, max_size=2**40
        ) as websocket:
            logger.debug("fetching available symbols")
            request = models.GetExchangeInfoRequest()
            await websocket.send(json.dumps(request, cls=Encoder))
            raw = await websocket.recv()

        try:
            data = json.loads(raw)
            exchange_info = models.GetExchangeInfoResponse.model_validate(data)
        except json.decoder.JSONDecodeError as ex:
            raise Exception("unparsable exchangeInfo response") from ex
        except pydantic.ValidationError as ex:
            raise Exception("unexpected exchangeInfo response format") from ex

        return {
            symbol_data.symbol: (symbol_data.from_currency, symbol_data.to_currency)
            for symbol_data in exchange_info.result.symbols
            if (
                (symbol_data.from_currency, symbol_data.to_currency) in currencies
                or (symbol_data.to_currency, symbol_data.from_currency) in currencies
            )
        }

    async def subscribe(
        self, symbols: list[str]
    ) -> AsyncIterator[models.AveragePriceInfoEvent]:
        async with websockets.connect(self._config.WS_STREAM_API_URL) as websocket:
            logging.debug("subscribing")
            subscription_request = models.SubscribeRequest.for_symbols(symbols)
            await websocket.send(json.dumps(subscription_request, cls=Encoder))

            async for raw in websocket:
                try:
                    data = json.loads(raw)
                except json.decoder.JSONDecodeError:
                    logger.warning("not a json data: %s", raw)
                    continue

                try:
                    message = models.parse(data)
                except pydantic.ValidationError as ex:
                    logger.warning("message parsing failed: %s. Ignoring", ex.errors)
                    continue

                if isinstance(message, models.SubscriptionResponse):
                    if message.result is None and message.id == subscription_request.id:
                        logger.info("subscribed successfully")
                        continue
                    logger.warning("unexpected subscription response: %s", message)
                    continue

                yield message
