import pydantic_settings


class BinanceSettings(pydantic_settings.BaseSettings):
    model_config = pydantic_settings.SettingsConfigDict(env_prefix="BINANCE_")

    WS_API_URL: str
    WS_STREAM_API_URL: str
