import pydantic_settings


class ConsumerSettings(pydantic_settings.BaseSettings):
    model_config = pydantic_settings.SettingsConfigDict(env_prefix="CONSUMER_")

    DB_DUMP_INTERVAL: int
    DB_CLEANUP_INTERVAL: int
    QUOTES_MAX_AGE: int
    RATES_PRECISION: int
    CONVERSIONS: str

    @property
    def conversions(self) -> list[tuple[str, str]]:
        return [tuple(pair.split(":", 1)) for pair in self.CONVERSIONS.split(",")]  # type: ignore[misc]
