import enum
import logging.config

import pydantic_settings


class LogLevel(enum.StrEnum):
    ERROR = enum.auto()
    WARNING = enum.auto()
    INFO = enum.auto()
    DEBUG = enum.auto()


class LogFormat(enum.StrEnum):
    JSON = enum.auto()
    PLAIN = enum.auto()


class LoggingSettings(pydantic_settings.BaseSettings):
    LOG_LEVEL: LogLevel = LogLevel.INFO


def configure_logging(config: LoggingSettings) -> None:
    logging_config = {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] [%(levelname)-8s] [%(name)s] %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            }
        },
        "handlers": {
            "default": {
                "formatter": "default",
                "class": logging.StreamHandler,
                "stream": "ext://sys.stdout",
                "level": "DEBUG",
            },
        },
        "loggers": {
            "root": {
                "level": config.LOG_LEVEL.upper(),
                "handlers": ["default"],
            },
            "crypto_converter": {
                "level": config.LOG_LEVEL.upper(),
                "handlers": ["default"],
                "propagate": False,
            },
            "websockets": {
                "level": logging.WARNING,
            },
        },
    }
    logging.config.dictConfig(logging_config)
