import datetime
import decimal
import uuid

import pytest
from crypto_converter.domain.converter import (
    ConverterService,
    OutdatedQuotesError,
    UnsupportedConversionError,
)
from crypto_converter.domain.models import Quote

from tests.fakes.quotes import FakeQuoteRepository


async def test_outdated_quotes() -> None:
    # arrange
    repository = FakeQuoteRepository()
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    expiration_interval = datetime.timedelta(minutes=1)
    precision = 6
    from_currency = "CUR1"
    to_currency = "CUR2"
    await repository.add(
        Quote(
            id=uuid.uuid4(),
            rate=decimal.Decimal(10),
            timestamp=now - expiration_interval - datetime.timedelta(seconds=1),
            from_currency=from_currency,
            to_currency=to_currency,
        ),
    )

    converter = ConverterService(
        expiration_interval=expiration_interval,
        quotes=repository,
        precision=precision,
    )
    # act
    # assert
    with pytest.raises(OutdatedQuotesError):
        _converted, _quote = await converter.convert(
            amount=decimal.Decimal(1),
            from_currency=from_currency,
            to_currency=to_currency,
            timestamp=now,
        )


async def test_missing_quotes() -> None:
    # arrange
    repository = FakeQuoteRepository()
    converter = ConverterService(
        expiration_interval=datetime.timedelta(minutes=1),
        quotes=repository,
        precision=6,
    )
    # act
    # assert
    with pytest.raises(UnsupportedConversionError):
        _converted, _quote = await converter.convert(
            amount=decimal.Decimal(1),
            from_currency="CUR1",
            to_currency="CUR2",
            timestamp=datetime.datetime.now(tz=datetime.timezone.utc),
        )


async def test_successful_conversion_with_rounding() -> None:
    # arrange
    repository = FakeQuoteRepository()
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    expiration_interval = datetime.timedelta(minutes=1)
    from_currency = "CUR1"
    to_currency = "CUR2"
    await repository.add(
        Quote(
            id=uuid.uuid4(),
            rate=decimal.Decimal("14.1329912456"),
            timestamp=now - expiration_interval // 2,
            from_currency=from_currency,
            to_currency=to_currency,
        ),
    )

    converter = ConverterService(
        expiration_interval=expiration_interval,
        quotes=repository,
        precision=6,
    )
    # act
    converted, _quote = await converter.convert(
        amount=decimal.Decimal("12.485"),
        from_currency=from_currency,
        to_currency=to_currency,
        timestamp=now,
    )
    assert converted == decimal.Decimal("176.450396")


async def test_successful_reverse_conversion_with_rounding() -> None:
    # arrange
    repository = FakeQuoteRepository()
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    expiration_interval = datetime.timedelta(minutes=1)
    from_currency = "CUR1"
    to_currency = "CUR2"
    await repository.add(
        Quote(
            id=uuid.uuid4(),
            rate=decimal.Decimal("14.1329912456"),
            timestamp=now - expiration_interval // 2,
            from_currency=from_currency,
            to_currency=to_currency,
        ),
    )

    converter = ConverterService(
        expiration_interval=expiration_interval,
        quotes=repository,
        precision=6,
    )
    # act
    converted, _quote = await converter.convert(
        amount=decimal.Decimal("176.450396"),
        from_currency=to_currency,
        to_currency=from_currency,
        timestamp=now,
    )
    assert converted == decimal.Decimal("12.485000")
