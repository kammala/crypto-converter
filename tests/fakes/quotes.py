import bisect
import collections
import datetime
from collections.abc import Callable

from crypto_converter.domain.models import Quote
from crypto_converter.storage.repository import QuoteRepository


class FakeQuoteRepository(QuoteRepository):
    _storage: dict[tuple[str, str], list[Quote]]
    _comparator: Callable[[Quote], datetime.datetime]

    def __init__(self) -> None:
        self._storage = collections.defaultdict(list)
        self._comparator = lambda q: q.timestamp

    async def get_latest_quote(
        self, *, from_currency: str, to_currency: str, timestamp: datetime.datetime
    ) -> Quote | None:
        quotes = self._storage[(from_currency, to_currency)]
        idx: int = bisect.bisect(quotes, timestamp, key=self._comparator)

        if idx <= 0:
            return None

        return quotes[idx - 1]

    async def add(self, quote: Quote) -> None:
        quotes = self._storage[(quote.from_currency, quote.to_currency)]
        bisect.insort(quotes, quote, key=self._comparator)
