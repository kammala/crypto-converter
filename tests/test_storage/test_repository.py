import dataclasses
import datetime
import decimal
import uuid
from collections.abc import AsyncIterator
from typing import cast

import asyncpg
import pytest
import sqlalchemy.ext.asyncio as sa_async
from crypto_converter.domain.models import Quote
from crypto_converter.storage.models import Base
from crypto_converter.storage.repository import PostgresQuoteRepository, QuoteRepository
from crypto_converter.storage.settings import StorageSettings

from tests.fakes.quotes import FakeQuoteRepository


@pytest.fixture
def fake() -> FakeQuoteRepository:
    return FakeQuoteRepository()


@pytest.fixture
def config() -> StorageSettings:
    return StorageSettings(
        POSTGRES_DB="tests",
    )


@pytest.fixture
async def with_database(config: StorageSettings) -> AsyncIterator[None]:
    connection = await asyncpg.connect(
        database="postgres",
        host=config.POSTGRES_HOST,
        port=config.POSTGRES_PORT,
        user=config.POSTGRES_USER,
        password=config.POSTGRES_PASSWORD,
    )
    await connection.execute(
        f"CREATE DATABASE {config.POSTGRES_DB} OWNER {config.POSTGRES_USER}"
    )
    await connection.close()

    engine = sa_async.create_async_engine(config.dsn)
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    await engine.dispose()

    try:
        yield
    finally:
        connection = await asyncpg.connect(
            database="postgres",
            host=config.POSTGRES_HOST,
            port=config.POSTGRES_PORT,
            user=config.POSTGRES_USER,
            password=config.POSTGRES_PASSWORD,
        )
        await connection.execute(f"DROP DATABASE IF EXISTS {config.POSTGRES_DB}")
        await connection.close()


@pytest.fixture
async def postgres(
    config: StorageSettings,
    with_database: None,  # noqa: ARG001
) -> AsyncIterator[PostgresQuoteRepository]:
    engine = sa_async.create_async_engine(config.dsn)
    session_factory = sa_async.async_sessionmaker(engine)
    async with session_factory() as session:
        yield PostgresQuoteRepository(session=session)
    await engine.dispose()


@pytest.fixture(params=["fake", "postgres"])
def repository(request: pytest.FixtureRequest) -> QuoteRepository:
    return cast(QuoteRepository, request.getfixturevalue(request.param))


async def test_get_latest_quotes(repository: QuoteRepository) -> None:
    # arrange
    currency_1 = "CUR1"
    currency_2 = "CUR2"
    first_quote = Quote(
        id=uuid.uuid4(),
        from_currency=currency_1,
        to_currency=currency_2,
        rate=decimal.Decimal("10"),
        timestamp=datetime.datetime(
            2024, 1, 1, 10, 10, 10, tzinfo=datetime.timezone.utc
        ),
    )
    second_quote = dataclasses.replace(
        first_quote,
        id=uuid.uuid4(),
        timestamp=first_quote.timestamp + datetime.timedelta(seconds=30),
        rate=decimal.Decimal("10.1"),
    )

    await repository.add(first_quote)
    await repository.add(second_quote)

    # act
    result = await repository.get_latest_quote(
        from_currency=currency_1,
        to_currency=currency_2,
        timestamp=second_quote.timestamp + datetime.timedelta(seconds=30),
    )

    # assert
    assert result == second_quote


async def test_get_quotes_atm(repository: QuoteRepository) -> None:
    # arrange
    currency_1 = "CUR1"
    currency_2 = "CUR2"
    first_quote = Quote(
        id=uuid.uuid4(),
        from_currency=currency_1,
        to_currency=currency_2,
        rate=decimal.Decimal("10"),
        timestamp=datetime.datetime(
            2024, 1, 1, 10, 10, 10, tzinfo=datetime.timezone.utc
        ),
    )
    second_quote = dataclasses.replace(
        first_quote,
        timestamp=first_quote.timestamp + datetime.timedelta(seconds=30),
        rate=decimal.Decimal("10.1"),
        id=uuid.uuid4(),
    )

    await repository.add(first_quote)
    await repository.add(second_quote)

    # act
    result = await repository.get_latest_quote(
        from_currency=currency_1,
        to_currency=currency_2,
        timestamp=second_quote.timestamp - datetime.timedelta(seconds=10),
    )

    # assert
    assert result == first_quote


async def test_get_missing_quotes(repository: QuoteRepository) -> None:
    # arrange
    # act
    result = await repository.get_latest_quote(
        from_currency="CUR1",
        to_currency="CUR2",
        timestamp=datetime.datetime.now(tz=datetime.timezone.utc),
    )
    # assert
    assert result is None
