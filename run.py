import argparse

import crypto_converter.api.app
import crypto_converter.quote_consumer.app
from crypto_converter.infra import logging


def main() -> None:
    # configure logging first
    logging.configure_logging(logging.LoggingSettings())

    parser = argparse.ArgumentParser(
        description="Run CryptoConverter modules",
    )
    parser.add_argument("module", choices=["api", "quote-consumer"])

    args = parser.parse_args()
    match args.module:
        case "api":
            crypto_converter.api.app.run()
        case "quote-consumer":
            crypto_converter.quote_consumer.app.run()
        case module:
            raise NotImplementedError(f"Module {module} is not yet implemented")


if __name__ == "__main__":
    main()
