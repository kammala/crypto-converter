FROM python:3.12.1-slim as prod

ENV POETRY_VERSION=1.7.1

RUN pip install "poetry==${POETRY_VERSION}"

WORKDIR /app
COPY pyproject.toml poetry.lock /app/
RUN poetry install --only main

COPY src/ /app/src/
COPY run.py /app
RUN poetry install --only-root


FROM prod as test
RUN poetry install --with dev
COPY .env/ /app/
COPY tests/ /app/tests/

FROM prod as migrations
COPY alembic.ini /app
RUN poetry install --with migrations
