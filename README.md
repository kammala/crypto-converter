# Crypto Converter
Binance Price API Scrapper with HTTP API to convert from one currency to another. 

## Requirements
- docker
- python
- [poetry](https://python-poetry.org/)
- [task](https://taskfile.dev/)

For concrete versions look at `.tool-versions` or use [asdf](https://github.com/asdf-vm/asdf) for setting up most of the project global requirements.

## Local development

Check `Taskfile.yaml` for all the commands available.

The most useful ones:
1. `task docker:up`: runs all the environment in docker compose
2. `task local:venv`: sets up local environment with project dependencies
3. `task lint`: runs checks like `mypy`, `ruff`
4. `task format`: runs autoformatters, fixers for lint issues
5. `task test`: runs tests, as there are tests depending on DB, make sure that you have started infra and configured `.env` files properly
6. `task docker:up:infra`: runs only infrastructural services in docker
7. `task local:run -- <whatever command>`: runs whatever command you are giving inside project environment — reads `.env` files and sets up `poetry` virtual env for commands.
- `task local:run -- alembic revision --autogenerate -m 'migration message'`
- `task local:run -- python run.py api`
- `task local:run -- python run.py quote-consumer`

When API is running you can check OpenAPI docs at `/docs` endpoint. 

## Configuration

The project is following 12-factor apps manifesto and is configurable via environment variables. By default `task` is reading env vars from `.env.local` and `.env` files, `.env.local` is excluded from version control, so you can use it to overwrite defaults for you local setup — this will mainly affect `task local:run` commands.

Applications running in `compose` do not read `.env.local` file, so if you want them to be affected, change `.env` file accordingly.

## TODOs
### Refactor quote consumer
- turn it into service with `start` and `stop` methods
- use some stop indicator, so background tasks (`quotes_janitor`, `db_dumper`) can stop properly
- add signal handlers that will stop service gracefully
- cover start/stop flows with tests

### Improve error handling
- background tasks will just silently die if smth goes wrong with DB, but the main consumer loop will live. we should either revive bg tasks or fail the whole process so that infra (like k8s) can restart it.
- websocket connections can die at any moment in time, we should handle it properly

### Improve tests
- some basic tests are in place, we can add more integration tests for infra (API and consumer)
- GitLab has nice integration with tests coverage, we can use that to prevent decreasing of tests' coverage
- think of some integration tests for consumer — it is not that easy to mock websockets.

### Improve monitoring
- add some prometheus metrics to API and quote consumer
